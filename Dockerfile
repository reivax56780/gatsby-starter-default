# Utilisez une image Node.js
FROM node:lts AS build

WORKDIR /app

COPY . .
# Installe les dépendances
RUN npm install -g gatsby-cli
RUN npm install
RUN gatsby build

#CMD ["gatsby", "develop", "-H", "0.0.0.0"]

FROM nginx

COPY --from=build /app/public /usr/share/nginx/html

#FROM httpd

#Copie les fichiers statiques de l'application Gatsby dans le dossier httpd
#COPY --from=build /app/public /usr/local/apache2/htdocs/


